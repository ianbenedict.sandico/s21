/*
JSON- data format used by application to store and transport data to one another, syntax, and a file type
Analogy: Data is the letter/information, JSON is the format and the casing of the letter

Data sending format- JSOn
Database - MongoDB

Database- is an organized collection of information or data
Data- is raw and does not collect any specific meaning
Information- is a group of organized data that contains logical meaning

Databases- typically refer to information stored in a computer syste, but it can also refer to physical databases
Library- database of books

Database management system- how database is managed,| how data is added, delete, etc

What is database management system?
- DBMS is specifically designed to manage the storage, retrieval and modification of data in the database.

Dewey Decimal System- How DBSM 

Query- a request with a very specific need, ex. find book with specific title, looking for specific category in ecommerce website
Querying database

Database systems allows four systems of operation when it comes to handling data, namely "CRUD:" 

Create(insert): create new users, new books

Read(select): find catalog of products, logging in, retrieving data

Update: data has to be maleable or changeable, 

Delete:

SQL or Relational database
-is a type of database where database is stored as a set of tables with rows and columns (pretty much a table printed on a piece of paper like excel)
- stands for structured query language
-used in typically relational DMBS to store, receive modify data
-code in SQL looks like this:

SELECT id, first_name, last_name FROM students WHERE batch_number = 1;
Batch_number- is the filter

Problem with SQL: 
-adding a column would mean added data to ALL
-will ruin format when you are adding something
-delete everything and start from scratch 

SQL databases: 
-requires table and information provided in its column to be defined before they are created and then used to store information. 
-information requires to be complete to prevent errors of storing data that are incomplete


NoSQL- Used for  unstructured data, cannot fit in tabular form
SQL offers flexibility, because it is unstructured and not relational. 
No SQL means NOT ONLY SQL.
NoSql was conceptualized when capturing complex, unstructured data became more difficult. 
Websites have become less strict throughtout the time.

What is MongoDB?
-is an opensource database and the leading NoSQL database
-is highly expressive, and generally friendly to those already familiar with JSON structure
-Highly expressive- if you want to do a specific command, you can freely do it. can make extremely specific queries
-Mongo means humungous

What does data stored in MongoDB look like?
-MongoDB is a database that uses key-value pairs, our data looks like this:
"_id" : object Id()
"firstName": "sylvan",
"lastname" : "cahilog",

These 3 look alike, because they are somewhat related:
Javascript- Logic
JSON- Sending
Mongo- Saving data

Main advantage of this data structure is that it is tored in MongoDB using JSON

Developer does not need to keep in mind the difference between writing code for the program itself and data access in a DBMS

In mongoDB, some terms and relational databases will be changed:

Database- is all the data
Tables --> collections; -pertains to a broad category of data that you can group together, organized data
Rows   --> documents;   - single piece of data in a collection
columns--> fields;      -specific information about the data. quantity, stock, price, name of product, color, size

Data Model Design

-Key decision in designing data models for MondoDB revolve around the structure of documents
And how the application represents relationships between data

References
-Store the relationship between data by including links or references from one doc to another

user document ---> contact document and access document

Referenced Data:
How to establish relationship between contact and access document to user document?
-From ID 
_id:<objectId1> ---> user_id <objectId1>,

-With refereced data, we can know that a document is relevant to another document

What if you want to get the email of the user?
1. get currently logged in users ID
2.use user_id to find the email

embedded cocuments
-capture relationships between data by storing related data in a SINGLE document structure
- we do it using Embedded sub-documents or objs within objs
-Easier way to query your data

1 negative of embedded documents
-it takes up space
-if you are expecting a document to expand to a huge size, DONT USE EMBEDDED DOCS

You can used BOTH;
1. Referenced
2. and Embedded
or you can put in logic to CLEAR orders when it piles up

FIRST STEP IN BUILDING AN APP OR CREATING SERVICE IS DATA SERVICE WHY?
-this is the step where you conceptualize your data, and it will define everything else later on.

*/
